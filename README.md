# Metabase quickstart

This is a quickstart for [Metabase](https://www.metabase.com/), an open source analytics tool that is compatible with Postgres and really is a great tool to derive and share insights from your data across your team/org. 
This quickstart builds on the Hasura `base` starter kit available at https://hasura.io/hub/projects/hasura/base ( which is essentially a blank template to be used as a starting point to build projects on Hasura).
It contains the following:

1. A `metabase` microservice that deploy the latest [Metabase] docker image.

2. Metabase has an embedded db to store it's state. However, it is highly recommended that the state be stored with persistence so all your queries are not lost. The aforementioned service also contains an init container that deploys a [docker image](https://hub.docker.com/r/face11301/create-db/) that contains a simple script to create a new database in the Postgres instance for this purpose.

3. Migrations to create an empty test table. So you can quickly verify if everything works after deploying the quickstart.



## How to deploy the quickstart

1. Clone this repo and cd into the top-level folder.

2. Add a new or an existing cluster to the project

3. git push hasura master. Wait a few minutes after running this. The aforementioned script needs to run and Metabase needs to do its thing. A couple of minutes at most should do it.

4. `hasura ms open metabase`. Sign up as the owner of this Metabase instance

5. Add the details of the Postgres instance that is part of the cluster, using the following values:
    
    a) Host: `postgres.hasura`

    b) Port: `5432`
    
    c) Database name: `hasuradb`
    
    d) Database username: `admin`
    
    e) Database password:
    
        i) Run `hasura secrets list` from inside your project directory
    
        ii) Use the value for the key `postgres.password`

Metabase should be ready to use in a few seconds.

